import { UserModel } from '../../models/user.dashboard.model';
import { ApiService } from '../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
})
export class CrudComponent implements OnInit {
  formValue!: FormGroup;
  userModel: UserModel = new UserModel();
  getUserData!: any;
  constructor(private formBuilder: FormBuilder, private api: ApiService) {}

  ngOnInit(): void {
    this.formValue = this.formBuilder.group({
      userName: [''],
      gender: [''],
      company: [''],
      age: [''],
    });
    this.getAll();
  }

  showAdd!: boolean;
  showUpdate!: boolean;

  getAll() {
    this.api.getUser().subscribe((res) => {
      this.getUserData = res;
    });
  }

  clickAddUser() {
    this.formValue.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }

  createUser() {
    this.userModel.userName = this.formValue.value.userName;
    this.userModel.gender = this.formValue.value.gender;
    this.userModel.company = this.formValue.value.company;
    this.userModel.age = this.formValue.value.age;
    this.api.postUser(this.userModel).subscribe(
      (res) => {
        alert('insert thành công');
        this.formValue.reset();
        this.getAll();
      },
      (err) => {
        alert(' errol');
      }
    );
  }

  editUser(item: any) {
    this.showAdd = false;
    this.showUpdate = true;
    this.userModel.id = item.id;
    this.formValue.controls['userName'].setValue(item.userName);
    this.formValue.controls['gender'].setValue(item.gender);
    this.formValue.controls['company'].setValue(item.company);
    this.formValue.controls['age'].setValue(item.age);
  }

  updateUser() {
    this.userModel.userName = this.formValue.value.userName;
    this.userModel.gender = this.formValue.value.gender;
    this.userModel.company = this.formValue.value.company;
    this.userModel.age = this.formValue.value.age;
    this.api
      .isUpdatelUser(this.userModel, this.userModel.id)
      .subscribe((res) => {
        alert('update thành công');
        this.formValue.reset();
        this.getAll();
      });
  }

  delUser(item: any) {
    this.api.deleteUser(item.id).subscribe((res) => {
      alert('delete thành công');
      this.getAll();
    });
  }
}
