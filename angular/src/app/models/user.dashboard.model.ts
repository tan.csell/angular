export class UserModel {
    id: number = 0;
    userName: string = '';
    gender: string = '';
    company: string = '';
    age: number = 0;
}
